/* global describe, it, beforeEach, afterEach */

(function () {
  'use strict';

  var chai = require('chai');
  var sinon = require('sinon');
  var assert = chai.assert;
  var Server = require('../../lib/Server.js');
  var Poll = require('../../lib/Poll.js');

  describe('Server', function () {

    var createNewUser = function(id) {
      return {
        'id' : id,
        'socket' : {
          'emit' : sinon.stub(),
          'broadcast' : {
            'emit' : sinon.stub()
          }
        }
      };
    };

    var resetUser = function (user) {
      user.socket.emit.reset();
      user.socket.broadcast.emit.reset();
    };

    var io = {
      'sockets' : {
        'emit' : sinon.stub()
      }
    };

    var options = {
      'timeout' : 30
    };

    var server = new Server(io, options);

    var user1 = createNewUser('id1');
    var user2 = createNewUser('id2');

    var pollDatas = {
      'title' : 'New poll',
      'choices' : ['one', 'two', 'three']
    };

    afterEach(function() {
      resetUser(user1);
      resetUser(user2);
      server.io.sockets.emit.reset();
    });

    describe('#onNewPoll', function () {

      it('should update current poll', function () {
        server.onNewPoll(user1, pollDatas);
        assert.equal(server.currentPoll.title, pollDatas.title);
        assert.equal(server.currentPoll.choices, pollDatas.choices);
      });

      it('should broadcast new poll and update status', function () {
        server.onNewPoll(user1, pollDatas);

        assert.isTrue(server.io.sockets.emit.calledWith('status', server.STATUS_POLL_IN_PROGRESS), 'new status is broadcasted');
        assert.isTrue(user1.socket.broadcast.emit.calledWith('newPoll'), 'new poll is broadcasted');

      });

      it('should start the current poll', function () {
        pollDatas.duration = 40;

        var startStub = sinon.stub(server.currentPoll, 'start');

        server.onNewPoll(user1, pollDatas);

        assert.isTrue(startStub.calledWith(40));
      });

    });

    describe('#onExtraTime', function () {

      it('should call poll addExtraTime and broadcast extraTime message with duration', function() {

        server.currentPoll = new Poll(user1, pollDatas.title, pollDatas.choices);

        var addExtraTimeStub = sinon.stub(server.currentPoll, 'addExtraTime');

        addExtraTimeStub.calledWith(10);

        server.onExtraTime(user1, 10);
        assert.isTrue(addExtraTimeStub.calledWith(10));
        assert.isTrue(server.io.sockets.emit.calledWith('extraTime', 10));

      });

    });

    describe('#onConnection', function () {

      it('should send current datas to the new connected user', function() {

        server.connectedUsers = 0;
        server.currentPoll = {
          'getDatas' : sinon.stub().returns({'foo' : 'bar'})
        };

        server.onConnection(user2);

        assert.isTrue(server.io.sockets.emit.calledWith('connectedUsers', 1), 'connectedUsers is updated and broadcasted');
        assert.isTrue(user2.socket.emit.calledWith('status'), 'status is sended to the user');

        assert.isTrue(user2.socket.emit.calledWith('newPoll', {'foo' : 'bar'}), 'new poll datas are sended to the user');
      });

    });

    describe('#onInitPoll', function () {
      it('should change status to … and send confirmation to user', function() {
        var callback = sinon.stub();

        // init status
        server.status = server.STATUS_INIT_POLL;

        server.onInitPoll(user1, null, callback);

        assert.isTrue(server.io.sockets.emit.calledWith('status', server.STATUS_CREATE_POLL), 'status is broadcasted');

        assert.isTrue(callback.calledWith(true), 'if status is INIT_POLL user reveive positive confirmation');

        // re-init callback
        callback.reset();

        // if another user try to init a poll
        server.onInitPoll(user2, null, callback);
        assert.isTrue(callback.calledWith(false), 'if status is not INIT_POLL user receive a negative confirmation');

      });
    });

    describe('#onDisconnect', function () {
      it('if poll author disconnect currentPoll should be ended', function() {

        server.currentPoll = new Poll(user1, pollDatas.title, pollDatas.choices);
        var StopPollStub = sinon.stub(server.currentPoll, 'stop');

        // init status
        server.status = server.STATUS_POLL_IN_PROGRESS;


        // user2 is not the current poll author so 'endCurrentPoll' must not be called
        server.onDisconnect(user2);
        assert.isTrue(StopPollStub.notCalled, 'on simple user disconnection endCurrentPoll must not be called');

        server.onDisconnect(user1);
        assert.isTrue(StopPollStub.calledOnce, 'on current poll author disconnection endCurrentPoll must be called');

        StopPollStub.restore();
      });

      it('on user disconnect new connected user number should be broadcasted', function() {
        server.connectedUsers = 1;

        server.onDisconnect(user2);

        assert.isTrue(server.io.sockets.emit.calledWith('connectedUsers', 0), 'new connected user number should be broadcasted');
      });


      it('when an author disconnect status should be updated to STATUS_INIT_POLL and broadcasted', function() {

        var user3 = {'id': 'id3'};

        server.currentPoll = {
          'author' : user3
        };

        server.status = server.STATUS_CREATE_POLL;

        server.onDisconnect(user3);

        assert.isTrue(server.io.sockets.emit.calledWith('status', server.STATUS_INIT_POLL));
      });

    });

    describe('#onNewVote', function () {

      beforeEach(function() {
        server.status = server.STATUS_POLL_IN_PROGRESS;
        server.currentPoll = new Poll(user1, pollDatas.title, pollDatas.choices);
      });

      afterEach(function() {
      });

      it('should register new vote', function () {
        var pollAnswerStub = sinon.stub(server.currentPoll, 'answer');
        var callback = sinon.stub();

        server.onNewVote(user2, 1, callback);

        // check vote has been registered
        assert.isTrue(pollAnswerStub.calledWith(user2, 1));

        // check callback call
        assert.isTrue(callback.calledWith(false));
      });

    });

    describe('#onEndPoll', function () {

      it('should call endCurrentPoll only if the user is the poll author', function () {

        server.currentPoll = new Poll(user1, pollDatas.title, pollDatas.choices);

        var stopPollStub = sinon.stub(server.currentPoll, 'stop');

        // if user is not the author
        server.onEndPoll(user2);
        assert.isTrue(stopPollStub.notCalled, 'if a user disconnect poll must not be ended');

        // if user is the author
        server.onEndPoll(user1);
        assert.isTrue(stopPollStub.calledOnce, 'if current poll author disconnect end the current poll');

        stopPollStub.restore();

      });

    });

    describe('#onReset', function () {

      it('should reset currentPoll and status and broadcast datas', function () {

        server.status = server.STATUS_POLL_ENDED;
        server.currentPoll = {
          'author' : user1,
          'foo' : 'bar'
        };

        server.onReset(user2);
        assert.equal(server.status, server.STATUS_POLL_ENDED, 'if the user is not the author do nothing');
        assert.equal(server.currentPoll.foo, 'bar', 'if the user is not the author do nothing');

        server.onReset(user1);
        assert.equal(server.status, server.STATUS_INIT_POLL, 'if the user is the current poll author update status');
        assert.isTrue(server.io.sockets.emit.calledWith('status', server.STATUS_INIT_POLL), 'if the user is the current poll author new status must be broadcasted');
        assert.isUndefined(server.currentPoll, 'if the user is the current poll author current poll must be empty');

      });

    });

  });

})();
